import sys
sys.path.append('./equation/functions')
sys.path.append('./data')

from equation import app 

if __name__ == '__main__':
    app.run()