import os, json
from equation import utils

def get_func_files(directory):
    files = os.listdir(directory)
    for file_ in files:
        if file_.endswith('.pyc'):
            files.remove(file_)
    if '' in files:
        files.remove('')
    if '__init__.py' in files:
        files.remove('__init__.py')
    if 'core.py' in files:
        files.remove('core.py')
    files = [x.replace('.py','') for x in files]
    return files

imports =  get_func_files('./equation/functions/')
print imports

modules = {}
for x in imports:
    print x
    try:
        modules[x] = __import__(x)
        print "Successfully imported ", x, '.'
    except ImportError:
        print "Error importing ", x, '.'

def exceuteFunc(function_name, input):
    custom_func = 'modules["'+function_name+'"].'+function_name+'('+str(input)+')'
    return eval(custom_func)

from flask_jsonpify import jsonpify
#extract useful fields and process them
def get_master_table_as_json():
    print '=== master_table() ==='
    master_table = utils.get_master_table()
    df_list = master_table.values.tolist()
    JSONP_data = jsonpify(df_list)
    return JSONP_data

def evaluate(function_name, input_list):
    print '=== evaluate(function_name, input_list) ===', function_name, ' | ', input_list
    val, msg = exceuteFunc(function_name, input_list)
    return val, msg
    