# -*- coding: utf-8 -*-
# I prefer to put each function into a separate file; they can be long and also it is better organized this way
def gfun_apple(x):
    import numpy as np
    x = np.array(x, dtype='f')

    g_val = 0.5 * sum(x)
    msg = 'Ok'
    return g_val, msg
