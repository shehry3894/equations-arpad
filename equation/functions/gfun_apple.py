# -*- coding: utf-8 -*-
# I prefer to put each function into a separate file; they can be long and also it is better organized this way
def gfun_apple(x):
    import numpy as np
    x = np.array(x, dtype='f')
    if not isinstance(x, np.ndarray):
        g_val   = float('nan')
        msg     = 'Incorrect input type, numpy.ndarray is expected'
    elif len(x) != 10:
        g_val 	= float('nan')
        msg 	= 'More or less inputs than expected'
    else:
        g_val 	= 1/2*sum(x)
        msg 	= 'Ok'
    return g_val, msg

