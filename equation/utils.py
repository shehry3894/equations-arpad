import pandas as pd
import numpy as np

master_table = pd.read_csv('./data/master-table.csv')

def get_master_table():
    return master_table
def get_function_name(set_id, problem_id):
    conditional_row =  master_table[(master_table.set_ID == set_id) & (master_table.problem_ID == problem_id)]
    function_name = conditional_row['function_name'].values[0]
    number_of_inputs = conditional_row['number_of_inputs'].values[0]
    return function_name, number_of_inputs


def validate_input(input_list, number_of_inputs):
    print '=== validate_input(input_list, number_of_inputs) ===', input_list, ' | ', number_of_inputs
    valid_input = False
    if isinstance(input_list, list):
        if len(input_list) == number_of_inputs:
            valid_input = True
        else:
            print '!!! INVALID LENGTH !!!'
            return valid_input
        if all(isinstance(x, float) for x in input_list):
            valid_input = True
        else:
            print '!!! INVALID DATA TYPE !!!'
            return valid_input

    else:
        print '!!! INPUT IS NOT A LIST !!!'

    return valid_input