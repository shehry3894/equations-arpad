import json
from equation import configs
from equation import utils
from equation.functions import core
from flask import Flask, request
import threading

app = Flask(__name__)


@app.route('/')
def main_page():
    return "Welcome to the BlackBox"


@app.route('/master-table')
def master_table():
    return core.get_table_as_json(configs.MASTER_TABLE_PATH)


@app.route('/auth-table')
def auth_table():
    return core.get_table_as_json(configs.AUTH_TABLE_PATH)


@app.route('/requests-count-table')
def requests_count_table():
    return core.get_table_as_json(configs.REQUESTS_COUNT_TABLE_PATH)


@app.route('/requests-table')
def requests_table():
    return core.get_table_as_json(configs.REQUESTS_TABLE_PATH)


@app.route('/evaluate', methods=["POST"])
def evaluate():
    print('=== /evaluate ===')
    res = "NONE"
    valid_json, req_json = utils.load_json(json.dumps(request.json))
    if not valid_json:
        return req_json
    # validate user
    username = req_json['username']
    password = req_json['password']
    if not utils.validate_user(username, password):
        return json.dumps({'val': float('nan'), 'msg': str('Invalid username/password.')})
    # validate function
    set_id = req_json['setID']
    problem_id = req_json['problemID']
    function_name, number_of_inputs = utils.get_function_name(set_id, problem_id)
    if not function_name:
        return json.dumps({'val': float('nan'), 'msg': str('Invalid setID/problemID.')})
    # validate request count
    if not utils.validate_request_count(username, set_id, problem_id):
        return json.dumps({'val': float('nan'), 'msg': str('Sorry, Request limit exceeded.')})
    print('=== valid input')
    # validate input
    input_list = req_json['inputList']
    valid_input, msg = utils.validate_input(input_list, number_of_inputs)
    val, msg = None, None
    if valid_input:
        val, msg = core.evaluate(function_name, input_list)
        res = json.dumps({'val': float(val), 'msg': str(msg)})
    else:
        res = json.dumps({'val': float('nan'), 'msg': str(msg)})

    threading.Thread(target=utils.add_request, args=(username, set_id, problem_id, val, input_list)).start()
    return res


def run():
    app.run(host='0.0.0.0', port=5000, debug=True)