import json

from equation import utils
from functions import core
from flask import Flask, request

app = Flask(__name__)

@app.route('/master_table')
def master_table():
    return core.get_master_table_as_json()


@app.route('/evaluate', methods=["POST"])
def evaluate():
    print '=== /evaluate ==='
    res = "NONE"
    req_json = json.loads(request.data)
    
    set_id = req_json['setID']
    problem_id = req_json['problemID']
    function_name, number_of_inputs = utils.get_function_name(set_id, problem_id)

    input_list = req_json['inputList']
    if utils.validate_input(input_list, number_of_inputs):
        print 'VALID INPUT'
        val, msg = core.evaluate(function_name, input_list)
        res = json.dumps({'val':float(val), 'msg':str(msg)})
    else:
        res = '!!! INVALID INPUT !!!'

    return res

def run():
    app.run(host='0.0.0.0', port=5000, debug=True)