MASTER_TABLE_PATH = './data/master-table.csv'
AUTH_TABLE_PATH = './data/auth-table.csv'
REQUESTS_TABLE_PATH = './data/requests-table.csv'
REQUESTS_COUNT_TABLE_PATH = './data/requests-count-table.csv'
FUNCTIONS_PATH = './equation/functions'
DATA_PATH = './data'

